#!/usr/bin/groovy
import groovy.json.JsonOutput

import java.nio.file.Paths

private CliBuilder cliBuilder() {
    CliBuilder cli = new CliBuilder(
            usage: 'log-work',
            header: '\nLog work against a JIRA ticket\n\n'
    )
    cli.h(longOpt: 'help', 'Print this message')
    cli._(longOpt: 'ticket', args: 1, argName: 'ticket_id', required: true, 'The ticket to log work against (required)')
    cli._(longOpt: 'time', args: 1, argName: 'jira_time_str', required: true, 'The time to log (required)')
    cli._(longOpt: 'comment', args: 1, argName: 'comment', required: false, 'A comment to add to the work log (optional)')
    cli._(longOpt: 'day', args: 1, argName: 'day', required: false, 'The day the work was started on (yyyy-MM-dd) (optional)')

    cli
}

def argsBuilder = cliBuilder()

if ("--help" in this.args || "-h" in this.args) {
    argsBuilder.usage()
    System.exit(0)
}

def options = argsBuilder.parse(this.args)
if (!options) {
    System.exit(1)
}
String ticket = options.ticket
String time = options.time
String comment = options.comment ? options.comment : null
String day = options.day ? options.day + "T10:00:00.000+0000" : null

def scriptDir = Paths.get(getClass().protectionDomain.codeSource.location.path).parent.toString()
def commonUtils = new GroovyScriptEngine(scriptDir).with {
    loadScriptByName('CommonUtils.groovy')
}

String jiraPropertiesFile = "$scriptDir/jira.properties"
commonUtils.ensureFileExists(jiraPropertiesFile)
Properties props = commonUtils.loadProperties(jiraPropertiesFile)

String jiraUrl = "${props.getProperty('jira.url')}/rest/api/2/issue/$ticket/worklog"
new URL(jiraUrl).openConnection().with {
    requestMethod = 'POST'
    doOutput = true
    setRequestProperty('Content-Type', 'application/json')
    setRequestProperty('Authorization', "Basic ${props.getProperty('jira.basic.authentication.token')}")

    outputStream.withPrintWriter {writer ->
        writer.write(JsonOutput.toJson([timeSpent: time, comment: comment, started: day]))
    }

    if (responseCode != 201) {
        System.err.println("An error occurred while logging work!")
        println inputStream.text
        System.exit(1)
    } else {
        println "Logged work successfully!"
        System.exit(0)
    }
}
