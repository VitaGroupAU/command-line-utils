static def ensureFileExists(String fileName) {
    def file = new File(fileName)
    if (!file.exists()) {
        System.err.println("Could not locate $fileName!")
        System.exit(1)
    }
}

static def loadProperties(String fileName) {
    Properties props = new Properties()
    new File(fileName).withInputStream {props.load(it)}

    props
}
