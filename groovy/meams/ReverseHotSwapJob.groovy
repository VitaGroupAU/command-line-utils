#!/usr/bin/groovy
import groovy.sql.Sql

private CliBuilder cliBuilder() {
    CliBuilder cli = new CliBuilder(usage: 'reverse-hot-swap')

    cli.h(longOpt: 'help', 'Print this message')
    cli._(longOpt: 'job-id' , args: 1, argName: 'job_id', required: true, 'The job ID')
    cli._(longOpt: 'asset-new-status', args: 1, argName: 'status', required: true, 'The new status for the replacement asset')
    cli._(longOpt: 'asset-new-substatus', args: 1, argName: 'status', required: true, 'The new substatus for the replacement asset')

    cli
}

Sql createMeamsProductionSql() {
    // Database details
    def meamsDbUrl = "jdbc:jtds:sqlserver://SRVDBS05:1433/EAMS;USENTLMV2=true;DOMAIN=fonezone;"
    def meamsDbDriver = "net.sourceforge.jtds.jdbc.Driver"
    def meamsDbUser = "MEAMSPD_SVC"
    def meamsDbPassword = "T3355921F07X5Gz6Qtmq1PMmu"
    def meamsSql = Sql.newInstance(meamsDbUrl, meamsDbUser, meamsDbPassword, meamsDbDriver)

    meamsSql
}

private Map getIds(Sql meamsSql, int jobId) {
    List result1 = meamsSql.rows("SELECT primary_asset_id, secondary_asset_id, profile_id FROM job where job_id=$jobId")
    if (result1.empty) {
        System.err.println("Could not find primary and secondary asset IDs for job: $jobId");
        System.exit(1)
    }
    if (result1.size() > 1) {
        System.err.println("Found more than one row for job: $jobId");
        System.exit(1)
    }
    int primaryAssetId = result1[0]["primary_asset_id"]
    int secondaryAssetId = result1[0]["secondary_asset_id"]
    int profileId = result1[0]["profile_id"]

    List result2 = meamsSql.rows("""
        select svc.service_id from asset a, sim s, service svc
        where a.asset_id = ${primaryAssetId}
        and a.sim_id = s.sim_id
        and s.service_id = svc.service_id
    """);
    if (result2.empty) {
        System.err.println("Could not find service ID for primary asset: $primaryAssetId");
        System.exit(1)
    }
    if (result2.size() > 1) {
        System.err.println("Found more than one service ID for primary asset: $primaryAssetId");
        System.exit(1)
    }
    int primaryServiceId = result2[0]["service_id"]

    List result3 = meamsSql.rows("select sim_id from asset where asset_id=$primaryAssetId")
    if (result3.empty) {
        System.err.println("Could not find sim ID for primary asset: $primaryAssetId");
        System.exit(1)
    }
    if (result3.size() > 1) {
        System.err.println("Found more than one sim ID for primary asset: $primaryAssetId");
        System.exit(1)
    }
    int primarySimId = result3[0]["sim_id"]

    List result4 = meamsSql.rows("select sim_id from asset where asset_id=$secondaryAssetId")
    if (result4.empty) {
        System.err.println("Could not find sim ID for secondary asset: $secondaryAssetId");
        System.exit(1)
    }
    if (result4.size() > 1) {
        System.err.println("Found more than one sim ID for secondary asset: $secondaryAssetId");
        System.exit(1)
    }
    int secondarySimId = result4[0]["sim_id"]

    List result5 = meamsSql.rows("select asset_tag from asset where asset_id = $primaryAssetId")
    if (result5.empty) {
        System.err.println("Could not find primary asset having ID: $primaryAssetId");
        System.exit(1)
    }
    if (result5.size() > 1) {
        System.err.println("Found more than one primary asset having ID: $primaryAssetId");
        System.exit(1)
    }
    String primaryAssetTag = result5[0]["asset_tag"]

    List result6 = meamsSql.rows("select asset_tag from asset where asset_id = $secondaryAssetId")
    if (result6.empty) {
        System.err.println("Could not find secondary asset having ID: $secondaryAssetId");
        System.exit(1)
    }
    if (result6.size() > 1) {
        System.err.println("Found more than one secondary asset having ID: $secondaryAssetId");
        System.exit(1)
    }
    String secondaryAssetTag = result6[0]["asset_tag"]

    return [
            primaryAssetId: primaryAssetId,
            secondaryAssetId: secondaryAssetId,
            profileId:profileId,
            primaryServiceId: primaryServiceId,
            primarySimId: primarySimId,
            secondarySimId: secondarySimId,
            primaryAssetTag: primaryAssetTag,
            secondaryAssetTag: secondaryAssetTag
    ]
}

private def echoSqlStatements(int jobId, Map ids, int newAssetStatus, int newAssetSubstatus) {
    println """
-- Ensure the job status = 385 (Provision Secondary Asset with Codes), job type = 13 (Replacement) and subtype = 11 (HotSwap).
select status, type, sub_type from job where job_id=$jobId;

-- Get the primary and secondary asset IDs and the profile ID for the job.
-- (PRIMARY_ASSET_ID=${ids['primaryAssetId']}, SECONDARY_ASSET_ID=${ids['secondaryAssetId']} and PROFILE_ID=${ids['profileId']})
select primary_asset_id, secondary_asset_id,profile_id from job where job_id=$jobId;

-- On the front end, navigate to the profile linked to the job and check that it has both the primary and secondary asset tags
-- listed under "Assets" and that the secondary asset tag does not have the option "Replacement".
-- (PRIMARY_ASSET_TAG=${ids['primaryAssetTag']}, SECONDARY_ASSET_TAG=${ids['secondaryAssetTag']})
select asset_tag as primary_asset_tag from asset where asset_id = ${ids['primaryAssetId']}; -- primary
select asset_tag as secondary_asset_tag from asset where asset_id = ${ids['secondaryAssetId']}; -- secondary

-- Cancel the job.
update job set status=999 where job_id = $jobId;

-- Unlink the primary asset from the profile and set it's status and substatus as per what's mentioned on the ticket.
update asset set profile_id = null, status = $newAssetStatus, substatus = $newAssetSubstatus where asset_id = ${ids['primaryAssetId']};

-- Get the service ID for the primary asset's sim (PRIMARY_ASSET_SERVICE_ID=${ids['primaryServiceId']}). Save this.
select svc.service_id from asset a, sim s, service svc
where a.asset_id = ${ids['primaryAssetId']}
and a.sim_id = s.sim_id
and s.service_id = svc.service_id;

-- On the primary sim, set service_id to NULL.
update sim set service_id = null
where sim_id = (
    select s.sim_id from asset a inner join sim s
    on a.sim_id=s.sim_id
    where a.asset_id = ${ids['primaryAssetId']}
);

-- On the secondary sim, set the service_id to PRIMARY_ASSET_SERVICE_ID and the released_date to NULL.
update sim set service_id = ${ids['primaryServiceId']}, released_date = null
where sim_id = (
    select s.sim_id from asset a inner join sim s
    on a.sim_id=s.sim_id
    where a.asset_id=${ids['secondaryAssetId']}
);

-- Revert the primary asset's sim.
-- Get primary asset's sim ID (PRIMARY_ASSET_SIM_ID=${ids['primarySimId']}).
select sim_id from asset where asset_id=${ids['primaryAssetId']};
-- Get primary sim's change log.
select * from audit_log where job_id=$jobId and entity_id=${ids['primarySimId']} and job_step='sim-swap'; -- should ideally return 1 row
-- Compare the "old_value" and "new_value" columns and revert any changes as necessary
-- (ignore the "lastModified" field; also ignore the "service" field as we've already set the service ID on the primary asset's sim to null).

-- Revert the service originally assigned to the primary asset (PRIMARY_ASSET_SERVICE_ID), which is now assigned back to the secondary asset.
select * from audit_log where job_id=$jobId and entity_id=${ids['primaryServiceId']} and job_step='sim-swap'; -- should ideally return 1 row
-- Compare the "old_value" and "new_value" columns and revert any changes as necessary (ignore the "lastModified" field).

-- Revert the secondary asset's sim.
-- Get secondary asset's sim ID (SECONDARY_ASSET_SIM_ID=${ids['secondarySimId']}).
select sim_id from asset where asset_id=${ids['secondaryAssetId']};
-- Get secondary sim's change log.
select * from audit_log where job_id=$jobId and entity_id=${ids['secondarySimId']} and job_step='sim-swap'; -- should ideally return 1 row
-- Compare the "old_value" and "new_value" columns and revert any changes as necessary
-- (ignore the "lastModified" field; also ignore the "service" field as we've have already set the service ID on the secondary asset's sim,
-- along with setting the released date to null).

-- Now, view the profile on Meams and make sure that
-- 1.) The primary asset is no longer visible on the profile.
-- 2.) The secondary asset has the "Replacement" option.
-- Also, view both the primary and secondary assets on the "Assets" tag and make sure that their status and substatus is as expected.
"""
}

def argsBuilder = cliBuilder()
if ("--help" in this.args || "-h" in this.args) {
    argsBuilder.usage()
    System.exit(0)
}

def options = argsBuilder.parse(this.args)
if (!options) {
    System.exit(1)
}

int jobId = Integer.valueOf(options["job-id"] as String)
int newAssetStatus = Integer.valueOf(options["asset-new-status"] as String)
int newAssetSubStatus = Integer.valueOf(options["asset-new-substatus"] as String)
Sql meamsSql = createMeamsProductionSql()
Map ids = getIds(meamsSql, jobId)

echoSqlStatements(jobId, ids, newAssetStatus, newAssetSubStatus)
